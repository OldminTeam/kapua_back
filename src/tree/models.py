from collections import OrderedDict

from django.db import models

from treebeard.mp_tree import MP_Node


class Node(MP_Node):
    title = models.CharField(max_length=30, default='', blank=True)
    subtitle = models.CharField(max_length=30, default='', blank=True)
    expanded = models.BooleanField(default=True)

    # default true, schema will be automatically created and
    # synced when it is saved
    auto_create_schema = True

    def __str__(self):
        return f'Node: {self.title}, {self.subtitle}, ' \
               f'{self.expanded}, {self.path}'

    @classmethod
    def get_node_by_id(cls, node_id):
        return cls.objects.get(pk=node_id)

    @classmethod
    def encode_tree(cls, dump):
        result = []
        for node_info in dump:
            curr_node = node_info['data']
            if 'children' in node_info:
                curr_node.update({'children':
                                      cls.encode_tree(node_info['children'])})
            else:
                curr_node.update({'children': []})
            result.append(curr_node)
        return result

    @classmethod
    def decode_tree(cls, tree):
        result = []
        for node_info in tree:
            curr_node = {}
            children = node_info.get('children')
            if children:
                curr_node.update({'children':
                                  cls.decode_tree(children)})
            curr_node.update({
                'data': OrderedDict([
                    ('title', node_info['title']),
                    ('subtitle', node_info['subtitle']),
                    ('expanded', node_info['expanded'])]),
            })
            result.append(curr_node)
        return result
