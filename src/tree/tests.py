import json
from collections import OrderedDict

from django.test import TestCase

from rest_framework.test import APITestCase, APIClient
from rest_framework.reverse import reverse

from .models import Node


def create_simple_tree():
    root = Node.add_root(title='Fa')
    Node.get_node_by_id(root.pk).add_child(title='Ch')
    Node.get_node_by_id(root.pk).add_sibling(title='Sb')
    return Node.objects.count()


class TreeViewTestCase(APITestCase):
    def test_update_to_empty_tree(self):
        create_simple_tree()

        client = APIClient()
        response = client.post(reverse('tree'), data={})

        self.assertEqual(response.data, {'success': True})
        self.assertEqual(client.get(reverse('tree')).data, [])

    def test_validation_of_missing_key(self):
        client = APIClient()
        response = client.post(reverse('tree'),
                               data={'title': 'Fa',
                                     'subtitle': ''})

        self.assertEqual(response.data, {'error': 'Missing key'})

    def test_list(self):
        create_simple_tree()

        client = APIClient()
        response = client.get(reverse('tree'))

        self.assertEqual(
            response.data,
            [
                {
                'title': 'Fa',
                'subtitle': '',
                'expanded': True,
                'children': [{
                    'title': 'Ch',
                    'subtitle': '',
                    'expanded': True,
                    'children': []}]
                },
                {
                    'title': 'Sb',
                    'subtitle': '',
                    'expanded': True,
                    'children': []
                }
            ]
        )


class EncodeTreeTestCase(TestCase):

    def test_encode(self):
        create_simple_tree()

        self.assertListEqual(
            Node.encode_tree(Node.dump_bulk()),
            [
                {
                'title': 'Fa',
                'subtitle': '',
                'expanded': True,
                'children': [{
                    'title': 'Ch',
                    'subtitle': '',
                    'expanded': True,
                    'children': []}]
                },
                {
                    'title': 'Sb',
                    'subtitle': '',
                    'expanded': True,
                    'children': []
                }
            ]
        )

class DecodeTreeTestCase(TestCase):
    def test_decode(self):
        self.assertListEqual(
            Node.decode_tree(
            [{
                'title': 'Fa',
                'subtitle': '',
                'expanded': True,
                'children': [{
                    'title': 'Ch',
                    'subtitle': '',
                    'expanded': True,
                    'children': []}]},
                {'title': 'Sb',
                 'subtitle': '',
                 'expanded': True,
                 'children': []}])
            ,
            [{'children': [{'data': OrderedDict([('title', 'Ch'),
                                                 ('subtitle', ''),
                                                 ('expanded', True)])}],
              'data': OrderedDict(
                  [('title', 'Fa'), ('subtitle', ''), ('expanded', True)])},
             {'data': OrderedDict(
                 [('title', 'Sb'), ('subtitle', ''), ('expanded', True)])}]
        )
