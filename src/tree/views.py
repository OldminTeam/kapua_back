import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Node


class TreeView(APIView):
    """
    List all nodes, or create a new tree.
    """
    def get(self, request, format=None):
        return Response(Node.encode_tree(Node.dump_bulk()))

    def post(self, request, format=None):
        try:
            tree = json.loads(request.body)
            decoded_tree = Node.decode_tree(tree)
            Node.objects.all().delete()
            Node.load_bulk(decoded_tree)
            return Response({'success': True},
                            status=status.HTTP_201_CREATED)
        except (KeyError, AttributeError):
            answer = {'error': 'Missing key'}
        except json.JSONDecodeError:
            answer = {'error': 'Wrong format'}
        return Response(answer, status=status.HTTP_400_BAD_REQUEST)
