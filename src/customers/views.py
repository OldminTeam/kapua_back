from django.conf import settings

from rest_framework import generics
from rest_framework.permissions import AllowAny

from .utils import create_tenant_if_not_exists
from .models import Domain
from .serializers import DomainSerializer


class DomainList(generics.ListCreateAPIView):
    queryset = Domain.objects.all().exclude(domain=settings.FRONTEND_DOMAIN)
    serializer_class = DomainSerializer
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        create_tenant_if_not_exists(serializer.validated_data['domain'])
