from django.conf import settings
from django.core.management.base import BaseCommand

from customers.models import Client, Domain


class Command(BaseCommand):
    help = 'Create tenant if not exists'

    def handle(self, *args, **options):
        tenant_data = {
            'schema_name': 'common_tree',
            'name': 'sorted tree',
            'paid_until': '2019-12-05',
            'on_trial': True
        }

        if not Client.objects.filter(**tenant_data).exists():
            tenant = Client(**tenant_data)
            tenant.save()

            domain = Domain()
            domain.domain = 'tenant.{}'.format(settings.FRONTEND_DOMAIN)
            domain.tenant = tenant
            domain.is_primary = True
            domain.save()
