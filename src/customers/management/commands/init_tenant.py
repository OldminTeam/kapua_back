from django.conf import settings
from django.core.management.base import BaseCommand

from customers.models import Client, Domain


class Command(BaseCommand):
    help = 'Initialize tenant if not exists'

    def handle(self, *args, **options):
        if not Client.objects.count():
            tenant = Client(schema_name='public',
                            name='Schemas Inc.',
                            paid_until='2020-12-05',
                            on_trial=False)
            tenant.save()

            domain = Domain()
            domain.domain = settings.FRONTEND_DOMAIN
            domain.tenant = tenant
            domain.is_primary = True
            domain.save()
