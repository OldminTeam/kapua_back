from django.urls import path

from .views import DomainList


urlpatterns = [
    path('list/', DomainList.as_view(), name='domain_list'),
]