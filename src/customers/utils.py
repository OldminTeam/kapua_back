from django.conf import settings

from .models import Client, Domain


def create_tenant_if_not_exists(domain_name):
    tenant_data = {
        'schema_name': '{}_tree'.format(domain_name),
        'name': '{}_sorted_tree'.format(domain_name),
        'paid_until': '2019-12-05',
        'on_trial': True
    }

    if not Client.objects.filter(**tenant_data).exists():
        tenant = Client(**tenant_data)
        tenant.save()
    else:
        tenant = Client.objects.filter(**tenant_data).get()

    if not Domain.objects.filter(domain=domain_name, tenant=tenant).exists():
        domain = Domain()
        domain.domain = '{}.{}'.format(domain_name, settings.FRONTEND_DOMAIN)
        domain.tenant = tenant
        domain.is_primary = True
        domain.save()
