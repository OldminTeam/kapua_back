from django.urls import path, include
from django.conf import settings

from tree.views import TreeView

urlpatterns = [
    path('tree/', TreeView.as_view(), name='tree'),
    path('domain/', include('customers.urls')),
]

if settings.DEBUG:
    urlpatterns += [
        path('api-auth/', include('rest_framework.urls'))
    ]

