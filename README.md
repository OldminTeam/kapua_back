# Run project
1. Install Docker (https://docs.docker.com/engine/installation/)
2. Install docker-compose (https://docs.docker.com/compose/install/)
3. The Postgres related data have to change to yours(in etc/docker/postgres.env and int etc/docker/web.env)
4. Build image and run containers:
```
docker-compose -d --build

```
